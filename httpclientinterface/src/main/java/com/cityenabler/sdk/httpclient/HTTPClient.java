package com.cityenabler.sdk.httpclient;

import java.util.List;
import java.util.Map;

public interface HTTPClient {
	
	public <T> T get(String uri, Map<String, List<String>> headers, Class<T> responseclass);
	
	public <T, V> T post(String uri, Map<String, List<String>> headers, V body, Class<T> responseclass);
	
	public <T, V> T put(String uri, Map<String, List<String>> headers, V body, Class<T> responseclass);
	
	public <T> T delete(String uri, Map<String, List<String>> headers, Class<T> responseclass);
	
	public <T> T head(String uri, Map<String, List<String>> headers, Class<T> responseclass);
	
}
